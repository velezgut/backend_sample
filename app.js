const { response } = require('express');
const express = require('express')
const api = require('./api/Api')
const app = express()
const port = 4500

app.use(express.static('public'));

/**landing page */
app.get('/', (req, res) => {
  //res.send('<h1>Zeitung Aboverkauf Home Page</h1>')
  res.sendFile( __dirname + "/" + "index.htm" );
})

/**System Types */
let abonnements = [
    {
        id: 1,
        name: 'Printed newspaper',
        description: 'Traditional paper printed newspaper, the most versatile product ever used in modern and past times. Read the news, contain accidental splits, kill plagues, train your pets, ... The perfect all-in-one product',
    },
    {
        id: 2,
        name: 'E-paper',
        description: 'Get the local, national and international news on your portable reader device, even when you are out.'
    },
    {
        id: 3,
        name: 'Website',
        description: 'Get access to the local, national and international news from any digital device.'
    },
]

let payments = [
    {
        id: 1,
        name: 'Monthly',
        description: 'One payment at the end of each month'
    },
    {
        id: 2,
        name: 'Annual',
        description: 'Simplify the payment, only once a year'
    }
];

let deliveries = [
    {
        id: 1,
        name: 'Post',
        description: ''
    },
    {
        id: 2,
        name: 'Delivery',
        description: ''
    }
];


app.get('/types/:requestedType', (req, res) => {
    console.log('type_request '+JSON.stringify(req.params));
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Content-Type: application/json')
    let param = req.params.requestedType;
    switch(param){
        case 'abonnements':   
            console.log('abonnements');   
            res.send(JSON.stringify(abonnements))
            break;
        case 'deliveries':
            console.log('deliveries');   
            res.send(JSON.stringify(deliveries));
            break;
        case 'payments':
            console.log('payments');   
            res.send(JSON.stringify(payments));
            break;
        default:
            let response = [
                {type:'abonnements', items: abonnements},
                {type:'deliveries', items: deliveries},
                {type:'payments', items: payments}
            ];
            res.send(JSON.stringify(response));
            break;
    }
}    
)
/**
 * Rest service to access the function: getDistanceFromCompanyToDestinationPlz
 */
app.get('/functions/getDistanceFromCompanyToDestinationPlz/:plzDestination', (req, res) => {
    console.log('functions_request '+JSON.stringify(req.params));
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Content-Type: application/json')
    let plzDestination = req.params.plzDestination;
    var promise = api.getDistanceFromCompanyToDestinationPlz(plzDestination);    
    promise.then((result) =>{
        console.log('result: '+JSON.stringify(result));
        res.send(JSON.stringify(result));
    });
    
}    
)

var server = app.listen(port, function () {
    var host = server.address().address
    var port = server.address().port
 
    console.log("App listening at http://%s:%s", host, port)
 })